﻿using System;
using Core.Interface;
using Services.Transactions;
using Services.Transfers;

namespace Rave.Apis
{
    public class RaveApi
    {
        public interface IRaveApi
        {
            ITransaction Transactions { get; }
            ITransfer Transfer { get; }

        }
        public RaveApi()
        {
            Transaction = new Transaction();
            Transfer = new Transfer();
        }

        public ITransaction Transaction { get; }
        public ITransfer Transfer { get; }
    }
}
