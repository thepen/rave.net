﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Core.Interface;
using System.Text.Json;
using Newtonsoft.Json.Serialization;

namespace Service.Https
{
    public class HttpHelper : IHttpHelper
    {
        private readonly HttpClient client;
        public HttpHelper()
        {
            client = new HttpClient();
        }
        public async Task<T> GetRequest<T>(string requesturl, string secreteKey = "")
        {
            var httpRequest = new HttpRequestMessage(HttpMethod.Get, requesturl);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await client.SendAsync(httpRequest);
            var responseBody = await response.Content.ReadAsStreamAsync();
            var result = await JsonSerializer.DeserializeAsync<T>(responseBody);
            return result;
        }

        public async Task<TOut> PostRequest<TIn, TOut>(string url, TIn content, string secreteKey = "")
        {
            var serialized = new StringContent(JsonSerializer.Serialize(content), Encoding.UTF8, "application/json");
            var response = await client.PostAsync(url, serialized);
            var responseBody = await response.Content.ReadAsStreamAsync();
            var result = await JsonSerializer.DeserializeAsync<TOut>(responseBody);  //JsonConvert.DeserializeObject<TOut>(responseBody);
            return result;
        }

        
    }
}
