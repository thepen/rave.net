﻿
using System.Threading.Tasks;
using Core.Helper;
using Core.Interface;
using Core.Models.RaveResponses;
using Core.Models.Transaction;
using Core.Pattern;
using Service.Https;

namespace Services.Transactions
{

    public class Transaction : ITransaction
    {
        private readonly IHttpHelper http;
        public Transaction()
        {
            http = new HttpHelper();
        }

        public Task<Response<VerifyTransactionResponse>> VerifyTransaction(string refCode, string secreteKey )
        {
            var verifyTransactionPayLoad = SimplFactory.CreateVerifyTransactionPayLoad(refCode, secreteKey);
            string url = Constants.BaseUrl + Constants.VerifyTransaction;
            var response = http.PostRequest<VerifyTransactionPayLoad, Response<VerifyTransactionResponse>>(url, verifyTransactionPayLoad, "");
            return response;
        }
    }


    
}
