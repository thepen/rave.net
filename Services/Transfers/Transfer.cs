﻿using System;
using System.Threading.Tasks;
using Core.Helper;
using Core.Interface;
using Core.Models.RaveResponses;
using Core.Models.Transfer;
using Core.Pattern;
using Service.Https;

namespace Services.Transfers
{
    public class Transfer : ITransfer
    {
        private readonly IHttpHelper _httpHelper;
        public Transfer()
        {
            _httpHelper = new HttpHelper();
        }

        public async Task<Response<InitiateTransferResponse>> InitiateTransfer(string reference, string secreteKey, string bankCode, string currency, int amount, string receipeientAccountNumber, string beneficiaryName, string narration, string callBackUrl = "", string recipient = "", string destinationBranchCode = "", string debitCurrency = "")
        {
            string url = Constants.BaseUrl + Constants.InitiateTransfer;
            var InitiateTransferPayLoad = SimplFactory.CreateInitiateTransferPayLoad( reference,  secreteKey,  bankCode,  currency,  amount,  receipeientAccountNumber,  beneficiaryName,  narration,  callBackUrl,  recipient,  destinationBranchCode ,  debitCurrency );
            var response = await _httpHelper.PostRequest<InitiateTransferPayLoad, Response<InitiateTransferResponse>>(url, InitiateTransferPayLoad, "");
            return response;
        }
    }
}
