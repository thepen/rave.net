﻿using System;
using System.Threading.Tasks;
using Rave.Apis;

namespace Consoles
{
    class Program
    {
        static async Task Main(string[] args)
        {
            RaveApi raveApi = new RaveApi();
            var result = await raveApi.Transaction.VerifyTransaction("MC-09182829", "FLWSECK-e6db11d1f8a6208de8cb2f94e293450e-X");
            Console.WriteLine(result.ToString());
        }


    }
}
