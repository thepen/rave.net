﻿# FlutterWave Rave Api for .Net Developers.
---
## This nuget package exposes all  endpoints from [rave payment documentaion](https://developer.flutterwave.com/) for .Net projects.

---
## How to Install

```Install-Package Rave.Net```

---

## Usage

---

```csharp
 using Rave.Net;   
```
```csharp
 
 RaveApi raveApi = new RaveApi();
 var result = await raveApi.Transaction.VerifyTransaction("txref", "SECKEY");    

```





