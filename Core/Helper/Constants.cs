﻿using System;
namespace Core.Helper
{
    public static class Constants
    {
        public const string BaseUrl = "https://api.ravepay.co/";
        public const string VerifyTransaction = "flwv3-pug/getpaidx/api/v2/verify";
        public const string InitiateTransfer = "v2/gpx/transfers/create";
    }
}
