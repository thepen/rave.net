﻿using System;
namespace Core.Interface
{
    public interface IRaveResponse<RaveResponse>
    {
        string status { get; set; }
        string message { get; set; }
        RaveResponse data { get; set; }
    }
}
