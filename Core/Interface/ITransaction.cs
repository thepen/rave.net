﻿using System;
using System.Threading.Tasks;
using Core.Models.RaveResponses;
using Core.Models.Transaction;

namespace Core.Interface
{
    public interface ITransaction
    {
        Task<Response<VerifyTransactionResponse>> VerifyTransaction(string refcode, string secreteKey );    
    }
}
