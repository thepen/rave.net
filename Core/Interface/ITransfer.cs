﻿using System;
using System.Threading.Tasks;
using Core.Models.RaveResponses;
using Core.Models.Transfer;

namespace Core.Interface
{
    public interface ITransfer
    {
        Task<Response<InitiateTransferResponse>> InitiateTransfer(string reference, string secreteKey,string bankCode,  string currency,int amount, string receipeientAccountNumber, string beneficiaryName, string narration, string callBackUrl = "", string recipient = "", string destinationBranchCode ="", string debitCurrency = "");

    }
}
