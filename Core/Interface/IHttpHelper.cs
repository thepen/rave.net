﻿using System;
using System.Threading.Tasks;

namespace Core.Interface
{
    public interface IHttpHelper
    {
        Task<T> GetRequest<T>(string requesturl, string secreteKey);
        Task<TOut> PostRequest<TIn, TOut>(string url, TIn content, string secreteKey);
    }
}
