﻿using System;
using Core.Interface;

namespace Core.Models.RaveResponses
{
    public class Response<RaveResponse> : IRaveResponse<RaveResponse>
    {
        public Response(RaveResponse response)
        {
            data = response;
        }

        public Response()
        {

        }

        public string status { get; set; }
        public string message { get; set; }
        public RaveResponse data { get; set; }
    }
}
