﻿using System;
namespace Core.Models.Transaction
{
    public class VerifyTransactionResponse
    {
        public int txid { get; set; }
        public double amount { get; set; }
        public double chargedamount { get; set; }
        public string txref { get; set; }
        public string flwref { get; set; }
        public string devicefingerprint { get; set; }
        public string cycle { get; set; }
        public string currency { get; set; }
        public string createdAt { get; set; }
        public string updatedAt { get; set; }
        public string deletedAt { get; set; }

    }
}
