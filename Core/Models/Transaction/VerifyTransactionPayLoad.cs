﻿using System;
namespace Core.Models.Transaction
{
    public class VerifyTransactionPayLoad
    {
        public string txref { get; set; }
        public string SECKEY { get; set; }
    }
}
