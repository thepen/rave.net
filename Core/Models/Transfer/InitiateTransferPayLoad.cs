﻿using System;
namespace Core.Models.Transfer
{
    public class InitiateTransferPayLoad
    {
        public string account_bank { get; set; }
        public string account_number { get; set; }
        public string recipient { get; set; }
        public  int amount { get; set; }
        public string narration { get; set; }
        public string currency { get; set; }
        public string seckey { get; set; }
        public string reference { get; set; }
        public string callback_url { get; set; }
        public string beneficiary_name { get; set; }
        public string destination_branch_code { get; set; }
        public string debit_currency { get; set; }
    }
}
