﻿using System;
using Core.Models.Transaction;
using Core.Models.Transfer;

namespace Core.Pattern
{
    public static class SimplFactory
    {
        public static InitiateTransferPayLoad CreateInitiateTransferPayLoad(string reference, string secreteKey, string bankCode, string currency, int amount, string receipeientAccountNumber, string beneficiaryName, string narration = "", string callBackUrl = "", string recipient = "", string destinationBranchCode = "", string debitCurrency = "")
        {
            InitiateTransferPayLoad InitiateTransferPayLoad = new InitiateTransferPayLoad
            {
                account_bank =  bankCode,
                account_number = receipeientAccountNumber,
                amount = amount,
                beneficiary_name = beneficiaryName,
                callback_url = callBackUrl,
                currency = currency,
                debit_currency = debitCurrency,
                destination_branch_code = destinationBranchCode,
                narration = narration,
                recipient = recipient,
                seckey =  secreteKey


                 
            };
            return InitiateTransferPayLoad;
        }
        public static VerifyTransactionPayLoad CreateVerifyTransactionPayLoad(string refText, string secKey)
        {
            VerifyTransactionPayLoad verifyTransactionPayLoad = new VerifyTransactionPayLoad
            {
                txref = refText,
                SECKEY = secKey
            };
            return verifyTransactionPayLoad;
        }
    }

   
}
